# OpenML dataset: naval_propulsion_plant

https://www.openml.org/d/44875

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

Data have been generated from a sophisticated simulator of a Gas Turbines (GT), mounted on a Frigate characterized by a COmbined Diesel eLectric And Gas (CODLAG) propulsion plant type.

The propulsion system behaviour has been described with this parameters:
- Ship speed (linear function of the lever position lp).
- Compressor degradation coefficient kMc.
- Turbine degradation coefficient kMt.
so that each possible degradation state can be described by a combination of this triple (lp,kMt,kMc).

A series of measures (16 features) which indirectly represents the state of the system subject to performance decay has been acquired and stored in the dataset over the parameter's space.

The goal is to estimate gt_compressor_decay_state_coefficient from the given measurements.

The columns gt_compressor_inlet_air_pressure, gt_compressor_inlet_air_temperature from the original dataset were
removed because they have constant values.


**Attribute Description**

1. *lever_position*
2. *ship_speed*
3. *gas_turbine_shaft_torque*
4. *gas_turbine_rate_of_revolutions*
5. *gas_generator_rate_of_revolutions*
6. *starboard_propeller_torque*
7. *port_propeller_torque*
8. *hp_turbine_exit_temperature*
9. *gt_compressor_inlet_air_temperature*
10. *gt_compressor_outlet_air_temperature*
11. *hp_turbine_exit_pressure*
12. *gt_compressor_inlet_air_pressure*
13. *gt_compressor_outlet_air_pressure*
14. *gas_turbine_exhaust_gas_pressure*
15. *turbine_injecton_control*
16. *fuel_flow*
17. *gt_compressor_decay_state_coefficient* - target feature
18. *gt_turbine_decay_state_coefficient* - alternate target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44875) of an [OpenML dataset](https://www.openml.org/d/44875). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44875/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44875/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44875/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

